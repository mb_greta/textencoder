package textencoder;

import java.util.Scanner;

public class Main {
	public static void main(String [] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Base text ?");
		String line = sc.nextLine();
		
		ScramblerMickael scm = new ScramblerMickael();
		line = scm.execute(line);
		
		System.out.println(line);
		
	}
}